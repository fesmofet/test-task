const http = require("http");
const mongoose = require('mongoose');
const { Client, PrivateKey } = require('dsteem');
const Data = require('./models/data');
const CronJob = require('cron').CronJob;
const client = new Client('https://api.steemit.com');

const writeToDBCron = new CronJob('0 0 */2 * * *', getPriceAndWriteToDB, null, true, 'Europe/Kiev');
const writeToSteemd = new CronJob('00 00 00 * * *', postToSteemd, null, true, 'Europe/Kiev');

async function start() {
    try {
      const url =`mongodb+srv://boris:Dlj8lw0cZep4FBe5@cluster0-hnwd6.mongodb.net/test?retryWrites=true&w=majority`
      await mongoose.connect(url, {useUnifiedTopology: true, useNewUrlParser: true});
      http.createServer().listen(3000)
    } catch(e) {
      console.log(e);
    }
}

start()
writeToDBCron.start();
writeToSteemd.start();

function getPriceAndWriteToDB() {
    client.database.getCurrentMedianHistoryPrice().then(result => {
        const data =  new Data({
            base: result.base.amount,
            quote: result.quote.amount,
            date: new Date().getTime()
        });
        try {
            data.save();
        } catch(e) {
            console.log(e);
        }
    });
}

function postToSteemd() {
    let base = 0; 
    let quote = 0;
    const today = new Date().getTime() - 86400000;
   
    Data.find()
    .then(data => {
        const lastDay = data.filter( item =>  today < item.date);
        lastDay.forEach(item => base += item.base);
        lastDay.forEach(item => quote += item.quote);
        const dataFromMongo = {
            avrBase: base / data.length,
            avrQuote: quote / data.length,
        };
        const postingKey = '5JrvPrQeBBvCRdjv29iDvkwn3EQYZ9jqfAHzrCyUvfbEbRkrYFC'
        const broadcastData = {
            required_auths: [],
            required_posting_auths: ['social'], 
            id: Math.random().toString(), 
            json: JSON.stringify(dataFromMongo), 
        };

        client.broadcast.json(broadcastData, PrivateKey.fromString(postingKey));
    })
}
