const {Schema, model} = require('mongoose')

const data = new Schema({
    base: Number,
    quote: Number,
    date: Number,
})

module.exports = model('Data', data)